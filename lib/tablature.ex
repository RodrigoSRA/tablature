defmodule Tablature do
  def parse(tab) do
    tab
    |> String.split("\n\n")
    |> Enum.map(fn segment -> parse_segment(segment) end)
    |> Enum.join(" ")
  end
  
  defp parse_line(line) do
    line
    |> String.split("")
    |> Enum.map(fn n -> {String.at(line,0),n} end)
    |> Enum.with_index()
    |> Enum.filter(fn {_a,b} -> rem(b,2) == 0 end)
    |> Enum.filter(fn {{_a,b},_c} -> b != "|" and b != "" end)
  end

  defp parse_segment(segment) do
    segment
    |> String.split
    |> Enum.map(fn linea -> parse_line(linea) end)
    |> List.flatten
    |> Enum.group_by(fn {_a,b} -> b end)
    |> Enum.sort_by(fn {a,_b} -> a end)
    |> Enum.map(fn {_a,b} -> b end)
    |> Enum.map(fn x -> Enum.map(x,fn {{a,b},_c} -> {a,b} end) end)
    |> Enum.map(fn x -> Enum.map(x,fn {a,b} -> if b =~ ~r/\d+/, do: a<>b, else: "_" end) end)
    |> Enum.map(fn x -> Enum.uniq_by(x,fn y -> y end) end)
    |> Enum.map(fn x -> if length(x) > 1, do: x -- ["_"], else: x end)
    |> Enum.map(fn x -> Enum.join(x,"/") end)
    |> Enum.join(" ")
  end
end
